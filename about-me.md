---
layout: doc
---
<script setup>
import { VPTeamMembers } from 'vitepress/theme'

const members = [
  {
    avatar: 'https://secure.gravatar.com/avatar/509177c260994e097da1649f4990f566?s=800&d=identicon',
    name: 'Jesus Santos',
    title: 'Creator',
    links: [
      { icon: 'github', link: 'https://github.com/aJesus37' },
      { icon: 'twitter', link: 'https://twitter.com/aJesus37' },
      { icon: 'linkedin', link: 'https://linkedin.com/in/andersonjsantos' }
    ]
  },
]
</script>

# About me

Olá 😀

Sou Jesus, mantenho esse blog. Minha ideia com ele é compartilhar o que sei e criar conteúdo que possa ajudar as pessoas.

Agora falando um pouco sobre mim… Eu tenho 21 anos, sou estudante de Segurança da Informação, moro na cidade de São Paulo. Sou grande apoiador da filosofia do Open Source/Software Livre, grande apoiador de disseminar conhecimento de forma gratuita, e um profundo apreciador das artes de infosec, bem como redes de computadores.

Passo a maior parte do meu tempo estudando como automatizar tarefas, como usar Linux em todas as coisas, e como simplificar tarefas com Docker. Ah, tudo isso ligado com segurança da informação, claro.

Quer falar comigo? Recomendo utilizar o telegram, com [esse link](https://t.me/jesusfromhellz) poderá me encontrar. Ou se preferir pode mandar um email pra mim, ajesus37@protonmail.com


<center><VPTeamMembers size="small" :members="members" /></center>