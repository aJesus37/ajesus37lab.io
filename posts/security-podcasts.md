---
Author: Jesus Santos
Updated: 2022-09-12 23:10
lastUpdated: true
---

# Podcasts - Because old is cool

> Lista de podcasts relacionados à segurança da informação, e formas de consumí-los

![](https://ajesus37.github.io/images/security-podcasts/mic-pic.jpg)
<!-- more -->

## Introdução

Se você nasceu depois dos anos 2000, as chances de você nunca ter ouvido falar sobre *podcasts* é grande, e se ouviu falar, certamente nunca ouviu um.

**Mas afinal, o que é um podcast?** *Podcasts* são como os atuais *vlogs*, mas sem a parte visual, ou seja, são *talkshows* totalmente em audio.

Sendo uma pessoa criada nos tempos atuais muito provavelmente te colocará na perspectiva de pessoas majoritariamente visuais, ou seja, os estímulos visuais te fazem sentir atraído por algo. Assim sendo, existem grandes chances de você não ter se interessado, à princípio, pela ideia de ouvir pessoas falando sem ver nada do que se passa, e isso que faz sentido, mas até o fim desse artigo você irá desistir dessa ideia.

Se você é da área de tecnologia, certamente já percebeu o quão valiosa é a língua inglesa dentro desse contexto, certo? Imagine então que você pode estudar não só o conteúdo que você regurlamente estuda, mas aprender inglês nesse processo. Maneiro, não?!

Sendo os *podcastas* totalmente em audio, você é forçado a prestar mais atenção no que você ouve, então acaba entendendo melhor, ou entendendo o contexto do que é dito em meio a palavras novas, o que te deixa próximo de entender o real significado dessa palavra, além de te fazer exercitar seus ouvidos à comunicação nessa língua.
Você não só pode, como deve agora, estar se perguntando: **não posso fazer a mesma coisa de um jeito mais divertido usando séries, filmes e músicas?** Bem, de fato você pode, mas você muito provavelmente não irá prestar tanta atenção nas falas enquanto lê legendas, ou enquanto curte a batida daquela música que tanto gosta. O conteúdo de um *podcast* está totalmente naquilo que se ouve, então se você não presta atenção o suficiente, você não entende. Ah! E depois de um tempo, você consegue ouvir os *podcasts* enquanto faz outras tarefas, como trabalhar, então você aproveita muito bem o seu tempo, enquanto se força a ficar mais focado no que está fazendo, além de ouvir o podcast e aprender com ele.

Se você é de fora da área de tecnologia, talvez o conteúdo à partir daqui esteja fora do que você busca. Aí vai o *spoiler*: uma lista com vários *podcasts* sobre segurança da informação pra você ouvir! E sim, todos são em inglês, *only God knows* o quão difícil é achar bons conteúdos dessa área na nossa língua, então fica a dica.

## 13 Podcasts de segurança da informação para ouvir e aprender

Os 3 primeiros estão em ordem da **minha** preferência, o restante fica a seu critério =)

### 1. **Malicious Life, by Cybereason**

- Se você gosta da história das coisas e gostaria de saber como grandes *hacks*, casos famosos ou interessantes casos relacionados à segurança da informação ocorreram, esse é tranquilamente minha primeira recomendação. Com diversas informações completas e por vezes direto de suas fontes, com especialistas convidados, o apresentador traz o conteúdo de forma muito fluida, sendo simples até pra quem é de fora da área de tecnologia.

Você pode acessá-lo [neste link](https://malicious.life/)

### 2. **Darknet Diaries**

- Muito semelhante ao *Malicious Life*, porém com uma pegada mais técnica e que lembra muito uma série. O apresentador te insere muito bem nos acontecimentos e traz os casos de forma excitante, quase como se o que é citado não fosse real. Traz também citações reais das pessoas que participaram de alguns casos, bem como comentários de especialistas.

Você pode acessá-lo [neste link](https://darknetdiaries.com/)

### 3. **Naked Security, by Sophos**

- Novidades, comentários pertinentes e atualização sobre o cenário de *Cybersecurity* e segurança em geral, esse seria o resumo rápido sobre esse *Podcast*. Conta com uma equipe de profissionais que trazem diversos tópicos relevantes e fresquinhos, com análises de ótima qualidade e com fontes confiáveis. Quer estar sempre atualizado? Esse podcast é um *must*.

Você pode acessá-lo [neste link](https://nakedsecurity.sophos.com/)

### 4 and forward...

[**Unsupervised Learning**](https://danielmiessler.com) - Compilado de conhecimento que um profissional de tecnologia traz em forma de audio. Conteúdo de qualidade não apenas sobre segurança, mas sobre tecnologia, pessoas, tendências, e o que mais caber ao gênero.

[**Risky Business**](https://risky.biz/) - Semanalmente um tema é escolhido e uma *talk* com especialistas acontece. Veja fatos relevantes e ponderações muito válidas de profissionais com muita experiência.

[**Defensive Security Podcast**](https://defensivesecurity.org/) - *Podcast* com foco em segurança defensiva, como brechas e estratégias para se defender.

[**The Privacy, Security, & OSINT Show**](https://inteltechniques.com/podcast.html) - *Podcast* sobre privacidade, segurança e técnicas de investigação online através de dados públicos.

[**Hacker Public Radio**](http://hackerpublicradio.org/) - Grupo aberto de indivíduos que se interessam por segurança e querem fazer *podcasts*. Muito conteúdo, nem sempre de qualidade sonora excepcional, mas que com certeza agrega muito se bem curado para os seus interesses.

[**Open Source Security Podcast**](https://www.opensourcesecuritypodcast.com/) - É segurança, é *open source*, com tópicos relevantes e atuais.

[**SANS Stormcast**](https://isc.sans.edu/podcast.html) - *Podcast* diário de curta duração, produzido todos os dias de semana, tem intuito de ser rápido e informativo quanto às mais recentes mudanças e novos conteúdos da área. (O inglês pode ser um pouco difícil de entender no início).

[**OWASP Podcast**](https://www.owasp.org/index.php/OWASP_Podcast) - *DevSecOps*? Quer estar por dentro do trabalho da galera que faz tantos projetos magníficos que é a OWASP? *Be my guest*.

[**The Cyber Wire**](https://thecyberwire.com/) - *Podcasts* focados (20~25 minutos) que tratam de forma concisa e resumida tópicos diversos relativos às novidades da área de *cybersecurity*.

[**Threat Wire**](https://www.hak5.org/category/episodes/threatwire) - Um dos *podcasts* de melhor qualidade que já vi. Infelizmente os episódios tem tamanhos muito grandes, então tive dificuldade de ouví-los, embora o funcionamento no *youtube* seja fabuloso. (Se tiver sorte com ele, me deixe um *feeback* aqui embaixo ;)).

## Como ouvir?

Existem por aí diversos aplicativos que tem como intuito servir como canal para que *podcasts* sejam ouvidos, seguidos e outras funcionalidades, sejam alguns pagos ou não.
Minha experiência chegou em 2 aplicativos, que funcionam tanto no Android como iOS (um deles), e são gratuitos!

### 1. [**RadioPublic**](https://radiopublic.com/) 

O que uso atualmente. Traz ótimas funcionalidades de *download* automático, uma *home* prática e notificações de novos episódios dos *podcasts* que sigo. 

### 2. **Podcast Addict**

Grande quantidade de funcionalidades e maior quantidade de *podcasts* nativos, suporta também vídeos. O que me faz usar o *RadioPublic* no lugar dele é sua interface não tão bonita, que é compensada pela qualidade do seu funcionamento.
Eles não possuem *site* próprio, então aqui vai o link para *download* [**Android**](https://play.google.com/store/apps/details?id=com.bambuna.podcastaddict) (Esse não está disponível pra *iOS* :( )


## O que fazer dentro dos apps?

Para adicionar os *podcasts*, basta clicar em adicionar *podcast* através de *URL/RSS*, ou colocar o *link* do *feed* na barra de pesquisa, depois ir à página do *podcast* e marcar para seguir/inscrever-se.

Segue a lista dos *feeds* para os *podcasts* apresentados acima:

[https://malicious.life/feed/podcast/](https://malicious.life/feed/podcast/) - Malicious life

[https://feeds.megaphone.fm/darknetdiaries](https://feeds.megaphone.fm/darknetdiaries) - Darknet Diaries

[https://nakedsecurity.sophos.com/feed/](https://nakedsecurity.sophos.com/feed/) - Naked Security

[https://omny.fm/shows/unsupervised-learning/playlists/podcast.rss](https://omny.fm/shows/unsupervised-learning/playlists/podcast.rss) - Unsupervised Learning

[https://risky.biz/feeds/risky-business/](https://risky.biz/feeds/risky-business/) - Risky Business

[https://defensivesecurity.org/feed/podcast](https://defensivesecurity.org/feed/podcast) - Defensive Security Podcast

[http://feeds.soundcloud.com/users/soundcloud:users:261098918/sounds.rss](http://feeds.soundcloud.com/users/soundcloud:users:261098918/sounds.rss) - The Privacy, Security, & OSINT Show

[http://hackerpublicradio.org/hpr_ogg_rss.php](http://hackerpublicradio.org/hpr_ogg_rss.php) - Hacker Public Radio

[https://opensourcesecuritypodcast.libsyn.com/rss](https://opensourcesecuritypodcast.libsyn.com/rss) - Open Source Security Podcast

[https://isc.sans.edu/dailypodcast.xml](https://isc.sans.edu/dailypodcast.xml) - SANS StormCast

[http://feeds.soundcloud.com/users/soundcloud:users:63303345/sounds.rss](http://feeds.soundcloud.com/users/soundcloud:users:63303345/sounds.rss) - OWASP Podcast

[http://thecyberwire.libsyn.com/rss](http://thecyberwire.libsyn.com/rss) - The Cyber Wire

[https://feed.podbean.com/shannonmorse/feed.xml](https://feed.podbean.com/shannonmorse/feed.xml) - Threat Wire

### Tem dúvidas, dicas ou outros podcasts pra essa lista? Comenta aqui embaixo =)