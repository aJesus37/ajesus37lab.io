export default {
    title: 'Heaven Labs',
    description: 'The house of Jesus.',
    lastUpdated: true,
    themeConfig: {
        nav: [
            { text: 'Posts', link: '/' },
            { text: 'Docs', link: '/docs/' },
            { text: 'About Me', link: '/about-me' }
        ],
        sidebar: {
            '/docs/': [
                {
                    text: 'Docs',
                    items: [
                        { text: 'Going on', link: '/docs/going-on.md' },
                        { text: 'Posts', link: '/' },
                        { text: 'About me', link: '/about-me' }
                    ]
                }
            ]
        },
        lastUpdated: 'Last Updated',
        editLink: {
            pattern: 'https://gitlab.com/aJesus37/ajesus37.gitlab.io/edit/main/:path',
            text: 'Edit this page on GitLab'
        },
    },
    footer: {
        message: 'Released under the MIT License.',
        copyright: 'Copyright © 2019-present Jesus'
    },
    markdown: {
        lineNumbers: true
    },
    outDir: './public'
}